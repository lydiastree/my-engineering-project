﻿using System;
using System.IO;
using System.Linq;
using System.Windows;

namespace PolewczykZofia_projektInzynierski
{
    public partial class TestSumUp : Window
    {
        public TestSumUp(int[] score, string patientName)
        {
            InitializeComponent();
            StreamWriter streamWriter = File.AppendText("RezultatyTestu.txt");
            streamWriter.Flush();
            streamWriter.Close();
            DateTime todaysDate = DateTime.Today;
            scoreTextBlock.Text = "Twój wynik to " + score.Sum().ToString() + " na 8 punktów! \n" +
                                   (score[0] + score[1]).ToString() + "/2 punkty za ćwiczenie słuchu fizycznego,\n" +
                                   (score[2] + score[3]).ToString() + "/2 punkty za ćwiczenie odtwarzania struktur dźwiękowych,\n" +
                                   (score[4] + score[5]).ToString() + "/2 punkty za ćwiczenie analizy zdaniowej,\n" +
                                   (score[6] + score[7]).ToString() + "/2 punkty za ćwiczenie rozpoznawania i tworzenia rymów. \n" +
                                   "\nTwój wynik został automatycznie zapisany, aby zobaczyć swój progres \nprzejdź do zakładki statystyki.";

            streamWriter = new StreamWriter("RezultatyTestu.txt", true);
            string patientData = " " + patientName + " " + (score[0] + score[1]).ToString() + " " + (score[2] + score[3]).ToString() + " " + (score[4] + score[5]).ToString()
                                                       + " " + (score[6] + score[7]).ToString() + " " + score.Sum() + " " + todaysDate.ToShortDateString() + " \n";
            streamWriter.Write(patientData);
            streamWriter.Flush();
            streamWriter.Close();
        }
    }
}