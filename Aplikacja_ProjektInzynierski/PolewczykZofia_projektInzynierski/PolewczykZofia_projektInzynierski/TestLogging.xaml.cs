﻿using System.Windows;

namespace PolewczykZofia_projektInzynierski
{
    public partial class TestLogging : Window
    {
        public TestLogging()
        {
            InitializeComponent();
        }

        private void TestStart_Click(object sender, RoutedEventArgs e)
        {
            PatientChoice patientChoice = new PatientChoice();
            patientChoice.Show();
        }

        private void NewPateint_Click(object sender, RoutedEventArgs e)
        {
            NewPatient newPatient = new NewPatient();
            newPatient.Show();
        }

        private void Staistics_Click(object sender, RoutedEventArgs e)
        {
            Statistics statistics = new Statistics();
            statistics.Show();
        }
    }
}