﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PolewczykZofia_projektInzynierski
{
    class HearingExerciseLoop
    {
        readonly Random random = new Random();
        int numberHelper;
        readonly string[] usedExamples = new string[4];
        readonly int[] anwserNumber = new int[4];

        public int[] NewExample(List<int> examplesList, string[] examples, int correctAnwser)
        {
            usedExamples[3] = examples[examplesList.First()];
            anwserNumber[3] = examplesList.First();

            for (int i = 0; i < anwserNumber.Length; i++)
            {
                do
                {
                    numberHelper = random.Next(0, examples.Length);
                    if (anwserNumber.Contains(numberHelper) || usedExamples.Contains(examples[numberHelper]))
                        numberHelper = examplesList.First();
                }
                while (numberHelper == examplesList.First());
                anwserNumber[i] = numberHelper;
                usedExamples[i] = examples[numberHelper];
            }
            anwserNumber[correctAnwser] = examplesList.First();

            return anwserNumber;
        }
    }
}