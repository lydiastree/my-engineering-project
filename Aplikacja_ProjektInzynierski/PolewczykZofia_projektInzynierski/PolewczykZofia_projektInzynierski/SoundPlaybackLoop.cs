﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PolewczykZofia_projektInzynierski
{
    class SoundPlaybackLoop
    {
        public int AnalyseRecordedSounds(float[] bufferExample, float[] bufferRecorded, long samplesExample, long samplesRecorded)
        {
            float thresholdExample = bufferExample.Max() * (float)(0.75);
            float thresholdRecorded = bufferRecorded.Max() * (float)(0.75);
            int pause = 0;
            const int pausestart = 3000;
            List<int> peaksExample = new List<int>();
            List<int> peaksRecorded = new List<int>();

            for (int i = 0; i < samplesExample; i++)
            {
                if (bufferExample[i] > thresholdExample && pause <= 0)
                {
                    peaksExample.Add(i);
                    pause = pausestart;
                }
                pause--;
            }
            pause = 0;

            for (int i = 0; i < samplesRecorded; i++)
            {
                if (bufferRecorded[i] > thresholdRecorded && pause <= 0)
                {
                    peaksRecorded.Add(i);
                    pause = pausestart;
                }
                pause--;
            }

            int similarity, difference = 0, thresholdRoznic = 12000;
            if (peaksExample.Count == peaksRecorded.Count)
            {
                similarity = 1;
                for (int i = 0; i < peaksExample.Count - 1; i++)
                    difference += Math.Abs((peaksExample[i] - peaksExample[i + 1]) - (peaksRecorded[i] - peaksRecorded[i + 1]));

                for (int i = 4; i >= 1; i--)
                    if (difference < thresholdRoznic * peaksExample.Count * i)
                        similarity++;
            }
            else
                similarity = 0;

            return similarity;
        }
    }
}
