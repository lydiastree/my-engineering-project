﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Windows;
using System.Windows.Media;

namespace PolewczykZofia_projektInzynierski
{
    public partial class SoundPlaybackExercise : Window
    { 
        SoundPlayer player;
        List<int> examplesList = new List<int>() { 0, 1, 2, 3, 4 };
        int soundExample, counter = 0;
        NAudio.Wave.WaveIn sourceStream = null;
        NAudio.Wave.WaveFileWriter waveFileWriter = null;
        private AudioFileReader audioFileExample, audioFileRecorded;
        private string filePath = null;
        private readonly string patientName;
        readonly string[] fileNames = new string[] { "TwoTimes.wav", "ThreeTimes.wav", "FourTimesR.wav", "FourTimesF.wav", "FiveTimes.wav" };
        readonly System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        private bool pressed = false;
        private RoutedEventArgs e;
        private readonly bool test;
        readonly int[] score;
        public SoundPlaybackExercise(bool test, int[] score, string patientName)
        {
            InitializeComponent();
            this.test = test;
            this.score = score;
            this.patientName = patientName;
            if (test == true)
                newExampleButton.Visibility = Visibility.Hidden;
            examplesList = examplesList.OrderBy(x => Guid.NewGuid()).ToList();
            soundExample = examplesList.First();
        }

        private void NewExampleButton_Click(object sender, RoutedEventArgs e)
        {
            examplesList.Remove(examplesList.First());
            if (!examplesList.Any())
            {
                examplesList = new List<int>() { 0, 1, 2, 3, 4 };
                examplesList = examplesList.OrderBy(x => Guid.NewGuid()).ToList();
            }
            soundExample = examplesList.First();
            startButton.Background = Brushes.WhiteSmoke;
            startButton.Foreground = Brushes.Black;
            startButton.Content = "START";
        }

        private void SoundButton_Click(object sender, RoutedEventArgs e)
        {
            Stream[] sounds = new Stream[] { Properties.Resources.TwoTimes, Properties.Resources.ThreeTimes, Properties.Resources.FourTimesR, Properties.Resources.FourTimesF, Properties.Resources.FiveTimes };
            player = new SoundPlayer(sounds[soundExample]);
            player.Play();
        }

        private void InstructionButton_Click(object sender, RoutedEventArgs e)
        {
            player = new SoundPlayer(Properties.Resources.SoundPlaybackInstr);
            player.Play();
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            if (pressed == false)
            {
                try
                {
                    pressed = true;
                    List<NAudio.Wave.WaveInCapabilities> microphones = new List<NAudio.Wave.WaveInCapabilities>();
                    for (int i = 0; i < NAudio.Wave.WaveIn.DeviceCount; i++)
                        microphones.Add(NAudio.Wave.WaveIn.GetCapabilities(i));

                    filePath = Path.Combine(System.IO.Path.GetFullPath(@"..\..\"), @"Resources\") + fileNames[soundExample];
                    sourceStream = new NAudio.Wave.WaveIn
                    {
                        DeviceNumber = microphones.IndexOf(microphones.First()),
                        WaveFormat = new NAudio.Wave.WaveFormat(44100, NAudio.Wave.WaveIn.GetCapabilities(microphones.IndexOf(microphones.First())).Channels)
                    };
                    sourceStream.DataAvailable += new EventHandler<NAudio.Wave.WaveInEventArgs>(SourceStream_DataAvailable);
                    waveFileWriter = new NAudio.Wave.WaveFileWriter("recorded.wav", sourceStream.WaveFormat);

                    timer.Interval = 5500;
                    sourceStream.StartRecording();
                    timer.Tick += new EventHandler(TimerElapsed);
                    timer.Start();
                    this.e = e;
                    startButton.Background = Brushes.Red;
                    startButton.Foreground = Brushes.White;
                    startButton.Content = "NAGRYWAM";
                }
                catch (Exception)
                {
                    startButton.Content = "Coś poszło nie tak";
                }
            }        
        }

        private void TimerElapsed(object sender, EventArgs ea)
        {
            timer.Stop();
            if (sourceStream != null)
            {
                sourceStream.StopRecording();
                sourceStream.Dispose();
                sourceStream = null;
            }
            if (waveFileWriter != null)
            {
                waveFileWriter.Dispose();
                waveFileWriter = null;
            }

            long samplesExample, samplesRecorded;
            float[] bufferExample, bufferRecorded;
            if (audioFileExample == null && audioFileRecorded == null)
            {
                audioFileExample = new AudioFileReader(filePath);
                samplesExample = audioFileExample.Length;
                bufferExample = new float[samplesExample];
                audioFileExample.Read(bufferExample, 0, (int)samplesExample);

                audioFileRecorded = new AudioFileReader("recorded.wav");
                samplesRecorded = audioFileRecorded.Length;
                bufferRecorded = new float[samplesRecorded];
                audioFileRecorded.Read(bufferRecorded, 0, (int)samplesRecorded);
            }
            else
            {
                samplesExample = 0;
                bufferExample = null;
                samplesRecorded = 0;
                bufferRecorded = null;
            }

            SoundPlaybackLoop soundPlaybackLoop = new SoundPlaybackLoop();
            int similarity = soundPlaybackLoop.AnalyseRecordedSounds(bufferExample, bufferRecorded, samplesExample, samplesRecorded);
            audioFileRecorded.Flush();
            audioFileRecorded.Close();
            audioFileRecorded = null;
            audioFileExample = null;

            if (test == false)
            {
                startButton.Background = Brushes.WhiteSmoke;
                if (similarity >= 4)
                {
                    startButton.Foreground = Brushes.Green;
                    startButton.Content = "DOBRZE!";
                    player = new SoundPlayer(Properties.Resources.correct);
                    player.Play();
                }
                else
                {
                    startButton.Foreground = Brushes.Red;
                    startButton.Content = "SPRÓBUJ JESZCZE RAZ";
                    player = new SoundPlayer(Properties.Resources.wrong);
                    player.Play();
                }
            }
            else
            {
                if(similarity >= 4)
                {
                    score[counter + 2] = 1;
                    counter++;
                    if (counter < 2)
                        NewExampleButton_Click(sender, e);
                    else if(counter == 2)
                    {
                        SentenceAnalysisExercise sentenceAnaliysisExercise = new SentenceAnalysisExercise(test, score, patientName);
                        sentenceAnaliysisExercise.Show();
                        Close();
                    }
                }
                else
                {
                    counter++;
                    if (counter < 2)
                        NewExampleButton_Click(sender, e);
                    else if(counter == 2)
                    {
                        SentenceAnalysisExercise sentenceAnaliysisExercise = new SentenceAnalysisExercise(test, score, patientName);
                        sentenceAnaliysisExercise.Show();
                        Close();
                    }
                }
            }
            pressed = false;
        }

        private void SourceStream_DataAvailable(object sender, NAudio.Wave.WaveInEventArgs e)
        {
            if (waveFileWriter == null) return;
            waveFileWriter.Write(e.Buffer, 0, e.BytesRecorded);
            waveFileWriter.Flush();
        }
    }
}