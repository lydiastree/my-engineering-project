﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Windows;
using System.Windows.Media.Imaging;

namespace PolewczykZofia_projektInzynierski
{
    public partial class SentenceAnalysisExercise : Window
    {
        readonly string[,] examples = new string[,] { { "sen1", "sen2", "sen3", "sen4", "sen5", "sen6", "sen7", "sen8", "sen9", "sen10" }, 
                                                {"2", "2", "3", "3", "4", "4", "5", "5", "6", "6" } };
        List<int> examplesList = new List<int>() { 0,1,2,3,4,5,6,7,8,9 };
        int soundExample, correctAnwser = 0, counter = 0;
        readonly Random random = new Random();
        private string anwserImageUri = null;
        private readonly string patientName;
        SoundPlayer player = new SoundPlayer();
        readonly bool test;
        readonly int[] score;

        public SentenceAnalysisExercise(bool test, int[] score, string patientName)
        {
            InitializeComponent();
            this.test = test;
            this.score = score;
            this.patientName = patientName;
            if (test == true)
                newExampleButton.Visibility = Visibility.Hidden;
            examplesList = examplesList.OrderBy(x => Guid.NewGuid()).ToList();
            soundExample = examplesList.First();
            correctAnwser = random.Next(0, 3);
            SentenceExerciseLoop sentenceExerciseLoop = new SentenceExerciseLoop();
            int[] anwsersNumber = sentenceExerciseLoop.NewExample(examples, examplesList, correctAnwser);

            anwserImageUri = "/images/" + examples[1, anwsersNumber[0]] + ".jpg";
            buttonOneImage.Source = new BitmapImage(new Uri(anwserImageUri, UriKind.Relative));

            anwserImageUri = "/images/" + examples[1, anwsersNumber[1]] + ".jpg";
            buttonTwoImage.Source = new BitmapImage(new Uri(anwserImageUri, UriKind.Relative));

            anwserImageUri = "/images/" + examples[1, anwsersNumber[2]] + ".jpg";
            buttonThreeImage.Source = new BitmapImage(new Uri(anwserImageUri, UriKind.Relative));
            examplesList.Remove(examplesList.First());
        }

        private void NewExampleButton_Click(object sender, RoutedEventArgs e)
        {
            if (!examplesList.Any())
            {
                examplesList = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
                examplesList = examplesList.OrderBy(x => Guid.NewGuid()).ToList();
            }
            examplesList = examplesList.OrderBy(x => Guid.NewGuid()).ToList();
            soundExample = examplesList.First();
            correctAnwser = random.Next(0, 3);
            SentenceExerciseLoop sentenceExerciseLoop = new SentenceExerciseLoop();
            int[] anwsersNumber = sentenceExerciseLoop.NewExample(examples, examplesList, correctAnwser);

            anwserImageUri = "/images/" + examples[1, anwsersNumber[0]] + ".jpg";
            buttonOneImage.Source = new BitmapImage(new Uri(anwserImageUri, UriKind.Relative));

            anwserImageUri = "/images/" + examples[1, anwsersNumber[1]] + ".jpg";
            buttonTwoImage.Source = new BitmapImage(new Uri(anwserImageUri, UriKind.Relative));

            anwserImageUri = "/images/" + examples[1, anwsersNumber[2]] + ".jpg";
            buttonThreeImage.Source = new BitmapImage(new Uri(anwserImageUri, UriKind.Relative));
            examplesList.Remove(examplesList.First());
        }

        private void SoundButton_Click(object sender, RoutedEventArgs e)
        {
            Stream[] sounds = new Stream[] { Properties.Resources.sen1, Properties.Resources.sen2, Properties.Resources.sen3, Properties.Resources.sen4, Properties.Resources.sen5,
                                            Properties.Resources.sen6, Properties.Resources.sen7, Properties.Resources.sen8, Properties.Resources.sen9, Properties.Resources.sen10};
            player = new SoundPlayer(sounds[soundExample]);
            player.Play();
        }

        private void InstructionButton_Click(object sender, RoutedEventArgs e)
        {
            player = new SoundPlayer(Properties.Resources.SentenceAnalysisInstr);
            player.Play();
        }

        private void ButtonOne_Click(object sender, RoutedEventArgs e)
        {
            if (test == false)
            {
                if (correctAnwser == 0)
                {
                    buttonOneImage.Source = new BitmapImage(new Uri("/images/correct.jpg", UriKind.Relative));
                    player = new SoundPlayer(Properties.Resources.correct);
                    player.Play();
                }
                else
                {
                    buttonOneImage.Source = new BitmapImage(new Uri("/images/wrong.jpg", UriKind.Relative));
                    player = new SoundPlayer(Properties.Resources.wrong);
                    player.Play();
                }
            }
            else
            {
                if (correctAnwser == 0)
                {
                    score[counter + 4] = 1;
                    counter++;
                    if (counter < 2)
                        NewExampleButton_Click(sender, e);
                    else
                    {
                        RhymeExercise rhymeExercise = new RhymeExercise(test, score, patientName);
                        rhymeExercise.Show();
                        Close();
                    }
                }
                else
                {
                    counter++;
                    if (counter < 2)
                        NewExampleButton_Click(sender, e);
                    else
                    {
                        RhymeExercise rhymeExercise = new RhymeExercise(test, score, patientName);
                        rhymeExercise.Show();
                        Close();
                    }
                }
            }
        }

        private void ButtonTwo_Click(object sender, RoutedEventArgs e)
        {
            if (test == false)
            {
                if (correctAnwser == 1)
                {
                    buttonTwoImage.Source = new BitmapImage(new Uri("/images/correct.jpg", UriKind.Relative));
                    player = new SoundPlayer(Properties.Resources.correct);
                    player.Play();
                }
                else
                {
                    buttonTwoImage.Source = new BitmapImage(new Uri("/images/wrong.jpg", UriKind.Relative));
                    player = new SoundPlayer(Properties.Resources.wrong);
                    player.Play();
                }
            }
            else
            {
                if (correctAnwser == 1)
                {
                    score[counter + 4] = 1;
                    counter++;
                    if (counter < 2)
                        NewExampleButton_Click(sender, e);
                    else
                    {
                        RhymeExercise rhymeExercise = new RhymeExercise(test, score, patientName);
                        rhymeExercise.Show();
                        Close();
                    }
                }
                else
                {
                    counter++;
                    if (counter < 2)
                        NewExampleButton_Click(sender, e);
                    else
                    {
                        RhymeExercise rhymeExercise = new RhymeExercise(test, score, patientName);
                        rhymeExercise.Show();
                        Close();
                    }
                }
            }
        }

        private void ButtonThree_Click(object sender, RoutedEventArgs e)
        {
            if (test == false)
            {
                if (correctAnwser == 2)
                {
                    buttonThreeImage.Source = new BitmapImage(new Uri("/images/correct.jpg", UriKind.Relative));
                    player = new SoundPlayer(Properties.Resources.correct);
                    player.Play();
                }
                else
                {
                    buttonThreeImage.Source = new BitmapImage(new Uri("/images/wrong.jpg", UriKind.Relative));
                    player = new SoundPlayer(Properties.Resources.wrong);
                    player.Play();
                }
            }
            else
            {
                if (correctAnwser == 2)
                {
                    score[counter + 4] = 1;
                    counter++;
                    if (counter < 2)
                        NewExampleButton_Click(sender, e);
                    else
                    {
                        RhymeExercise rhymeExercise = new RhymeExercise(test, score, patientName);
                        rhymeExercise.Show();
                        Close();
                    }
                }
                else
                {
                    counter++;
                    if (counter < 2)
                        NewExampleButton_Click(sender, e);
                    else
                    {
                        RhymeExercise rhymeExercise = new RhymeExercise(test, score, patientName);
                        rhymeExercise.Show();
                        Close();
                    }
                }
            }
        }
    }
}