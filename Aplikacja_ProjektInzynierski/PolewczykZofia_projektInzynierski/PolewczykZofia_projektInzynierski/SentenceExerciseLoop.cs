﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PolewczykZofia_projektInzynierski
{
    class SentenceExerciseLoop
    {
        readonly Random random = new Random();
        int numberHelper;
        readonly int[] anwserNumber = new int[4];
        readonly string[] usedExamples = new string[4];

        public int[] NewExample(string[,] examples, List<int> exampleList, int correctAnwser)
        {
            usedExamples[3] = examples[1, exampleList.First()];
            anwserNumber[3] = exampleList.First();
            for (int i = 0; i < anwserNumber.Length - 1; i++)
            {
                do
                {
                    numberHelper = random.Next(0, 10);
                    if (anwserNumber.Contains(numberHelper) || usedExamples.Contains(examples[1, numberHelper]))
                        numberHelper = int.Parse(examples[1, exampleList.First()]);

                } while (numberHelper == int.Parse(examples[1, exampleList.First()]));

                anwserNumber[i] = numberHelper;
                usedExamples[i] = examples[1, numberHelper];
            }
            anwserNumber[correctAnwser] = exampleList.First();

            return anwserNumber;
        }
    }
}