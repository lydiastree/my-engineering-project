﻿using System.Linq;
using System.Windows;
using System.IO;

namespace PolewczykZofia_projektInzynierski
{
    public partial class PatientChoice : Window
    {
        readonly bool test = true;
        readonly int[] score = new int[8];
        public PatientChoice()
        {
            InitializeComponent();
            StreamWriter streamWriter = File.AppendText("Pacjenci.txt");
            streamWriter.Flush();
            streamWriter.Close();
            StreamReader streamReader = new StreamReader("Pacjenci.txt");
            string patients = streamReader.ReadToEnd();
            string[] patientsInfo = patients.Split(' ');
            string[] patientsNames = new string[patientsInfo.Length];
            for (int i = 1; i < patientsInfo.Length; i+=2)
            {
                patientsNames[i] = patientsInfo[i];
            }
            patientsNames = patientsNames.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            streamReader.Close();
            patientComboBox.ItemsSource = patientsNames;
        }

        private void TestStart_Click(object sender, RoutedEventArgs e)
        {
            if (patientComboBox.SelectedItem == null)
                infoTextBlock.Text = "Nie wybrano pacjenta";
            else
            {
                string patientName = patientComboBox.SelectedItem.ToString();
                PhysicalHearingExercise physicalHearingExercise = new PhysicalHearingExercise(test, score, patientName);
                physicalHearingExercise.Show();
                Close();
            }
        }

        private void PatientComboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            infoTextBlock.Text = "";
        }
    }
}