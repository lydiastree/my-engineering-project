﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Windows;
using System.Windows.Media.Imaging;

namespace PolewczykZofia_projektInzynierski
{
    public partial class RhymeExercise : Window
    {
        public string[,] examples = new string[,]{{"poppy", "hairdryer", "cat", "water", "hammer","hook", "mower", "log", "fence", "elephant", "horse", "pear", "can", "boat", "lock", "" , "", "", "", "", "", ""}, 
                                                  {"hook", "mower", "fence", "log", "cat", "poppy", "hairdryer", "water", "cat", "horse", "elephant", "can", "pear", "lock", "boat", "bell", "clock", "dog", "newspaper", "parrot", "pear", "phone"} };
        public Random random = new Random();
        List<int> examplesList = new List<int> { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 };
        private int counter = 0, correctAnwser = 0;
        private string anwserImageUri = null;
        private readonly string patientName;
        SoundPlayer player;
        readonly bool test;
        readonly int[] score;

        public RhymeExercise(bool test, int[] score, string patientName)
        {
            InitializeComponent();
            this.test = test;
            this.score = score;
            this.patientName = patientName;
            if (test == true)
                newExampleButton.Visibility = Visibility.Hidden;

            examplesList = examplesList.OrderBy(x => Guid.NewGuid()).ToList();
            int soundExample = examplesList.First();
            string inputImageUri = "/images/" + examples[0, examplesList.First()] + ".jpg";
            inputImage.Source = new BitmapImage(new Uri(inputImageUri, UriKind.Relative));

            correctAnwser = random.Next(0, 3);
            RhymeExerciseLoop rhymeExerciseLoop = new RhymeExerciseLoop();
            int[] anwserNumber = rhymeExerciseLoop.NewExample(examplesList, examples, correctAnwser);

            anwserImageUri = "/images/" + examples[1, anwserNumber[0]] + ".jpg";
            buttonOneImage.Source = new BitmapImage(new Uri(anwserImageUri, UriKind.Relative));

            anwserImageUri = "/images/" + examples[1, anwserNumber[1]] + ".jpg";
            buttonTwoImage.Source = new BitmapImage(new Uri(anwserImageUri, UriKind.Relative));

            anwserImageUri = "/images/" + examples[1, anwserNumber[2]] + ".jpg";
            buttonThreeImage.Source = new BitmapImage(new Uri(anwserImageUri, UriKind.Relative));
            examplesList.Remove(examplesList.First());
        }

        private void NewExampleButton_Click(object sender, RoutedEventArgs e)
        {
            if (!examplesList.Any())
            {
                examplesList = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 };
                examplesList = examplesList.OrderBy(x => Guid.NewGuid()).ToList();
            }
            int soundExample = examplesList.First();
            string inputImageUri = "/images/" + examples[0, examplesList.First()] + ".jpg";
            inputImage.Source = new BitmapImage(new Uri(inputImageUri, UriKind.Relative));

            correctAnwser = random.Next(0, 3);
            RhymeExerciseLoop rhymeExerciseLoop = new RhymeExerciseLoop();
            int[] anwserNumber = rhymeExerciseLoop.NewExample(examplesList, examples, correctAnwser);

            anwserImageUri = "/images/" + examples[1, anwserNumber[0]] + ".jpg";
            buttonOneImage.Source = new BitmapImage(new Uri(anwserImageUri, UriKind.Relative));

            anwserImageUri = "/images/" + examples[1, anwserNumber[1]] + ".jpg";
            buttonTwoImage.Source = new BitmapImage(new Uri(anwserImageUri, UriKind.Relative));

            anwserImageUri = "/images/" + examples[1, anwserNumber[2]] + ".jpg";
            buttonThreeImage.Source = new BitmapImage(new Uri(anwserImageUri, UriKind.Relative));
            examplesList.Remove(examplesList.First());
        }

        private void InstructionButton_Click(object sender, RoutedEventArgs e)
        {
            player = new SoundPlayer(Properties.Resources.RhymeInstr);
            player.Play();
        }

        private void ButtonOne_Click(object sender, RoutedEventArgs e)
        {
            if (test == false)
            {
                if (correctAnwser == 0)
                {
                    buttonOneImage.Source = new BitmapImage(new Uri("/images/correct.jpg", UriKind.Relative));
                    player = new SoundPlayer(Properties.Resources.correct);
                    player.Play();
                }
                else
                {
                    buttonOneImage.Source = new BitmapImage(new Uri("/images/wrong.jpg", UriKind.Relative));
                    player = new SoundPlayer(Properties.Resources.wrong);
                    player.Play();
                }
            }
            else
            {
                if (correctAnwser == 0)
                {
                    score[counter + 6] = 1;
                    counter++;
                    if (counter < 2)
                        NewExampleButton_Click(sender, e);
                    else
                    {
                        TestSumUp testSumUp = new TestSumUp(score, patientName);
                        testSumUp.Show();
                        Close();
                    }
                }
                else
                {
                    counter++;
                    if (counter < 2)
                        NewExampleButton_Click(sender, e);
                    else
                    {
                        TestSumUp testSumUp = new TestSumUp(score, patientName);
                        testSumUp.Show();
                        Close();
                    }
                }
            }
        }

        private void ButtonTwo_Click(object sender, RoutedEventArgs e)
        {
            if (test == false)
            {
                if (correctAnwser == 1)
                {
                    buttonTwoImage.Source = new BitmapImage(new Uri("/images/correct.jpg", UriKind.Relative));
                    player = new SoundPlayer(Properties.Resources.correct);
                    player.Play();
                }
                else
                {
                    buttonTwoImage.Source = new BitmapImage(new Uri("/images/wrong.jpg", UriKind.Relative));
                    player = new SoundPlayer(Properties.Resources.wrong);
                    player.Play();
                }
            }
            else
            {
                if (correctAnwser == 1)
                {
                    score[counter + 6] = 1;
                    counter++;
                    if (counter < 2)
                        NewExampleButton_Click(sender, e);
                    else
                    {
                        TestSumUp testSumUp = new TestSumUp(score, patientName);
                        testSumUp.Show();
                        Close();
                    }
                }
                else
                {
                    counter++;
                    if (counter < 2)
                        NewExampleButton_Click(sender, e);
                    else
                    {
                        TestSumUp testSumUp = new TestSumUp(score, patientName);
                        testSumUp.Show();
                        Close();
                    }
                }
            }
        }

        private void ButtonThree_Click(object sender, RoutedEventArgs e)
        {
            if (test == false)
            {
                if (correctAnwser == 2)
                {
                    buttonThreeImage.Source = new BitmapImage(new Uri("/images/correct.jpg", UriKind.Relative));
                    player = new SoundPlayer(Properties.Resources.correct);
                    player.Play();
                }
                else
                {
                    buttonThreeImage.Source = new BitmapImage(new Uri("/images/wrong.jpg", UriKind.Relative));
                    player = new SoundPlayer(Properties.Resources.wrong);
                    player.Play();
                }
            }
            else
            {
                if (correctAnwser == 2)
                {
                    score[counter + 6] = 1;
                    counter++;
                    if (counter < 2)
                        NewExampleButton_Click(sender, e);
                    else
                    {
                        TestSumUp testSumUp = new TestSumUp(score, patientName);
                        testSumUp.Show();
                        Close();
                    }
                }
                else
                {
                    counter++;
                    if (counter < 2)
                        NewExampleButton_Click(sender, e);
                    else
                    {
                        TestSumUp testSumUp = new TestSumUp(score, patientName);
                        testSumUp.Show();
                        Close();
                    }
                }
            }
        }
    }
}