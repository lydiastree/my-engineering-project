﻿using System;
using System.IO;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace PolewczykZofia_projektInzynierski
{
    public class TestPlot
    {
        public TestPlot()
        {
            FunctionSeries data;
            this.Model = new PlotModel { Title = "Postępy Pacjenta" };
            StreamReader streamReader = null;

            if (File.Exists("DanePacjenta.txt") == true)
            {
                streamReader = new StreamReader("DanePacjenta.txt");
                string readData = streamReader.ReadToEnd();
                streamReader.Close();
                string[] readDataSplit = readData.Split(' ');
                int sampleCount = readDataSplit.Length - 2;

                if (readDataSplit[0] == "test")
                {
                    int testCount = (readDataSplit.Length - 3) / 2;
                    var plotData = new FunctionSeries() { Color = OxyColor.FromRgb(102, 153, 255) };
                    OxyPlot.Axes.LinearAxis xAxis = new LinearAxis { Position = AxisPosition.Bottom, Title = "Numer podejścia do testu", Maximum = testCount + 1, Minimum = 0, MinimumMajorStep = 1, MinimumMinorStep = 1 };
                    OxyPlot.Axes.LinearAxis yAxis = new LinearAxis { Position = AxisPosition.Left, Title = "Wynik", Maximum = 9, Minimum = 0, MinimumMajorStep = 1, MinimumMinorStep = 1 };

                    for (int i = 0; i < testCount; i++)
                        plotData.Points.Add(new DataPoint(i + 1, int.Parse(readDataSplit[i + 2])));

                    this.Model.Axes.Add(xAxis);
                    this.Model.Axes.Add(yAxis);
                    this.Model.Series.Add(plotData);

                }
                else if (readDataSplit[0] == "ph")
                {
                    var physicalResult = new FunctionSeries() { Title = "Ćwiczenie sł. fiycznego", Color = OxyColor.FromRgb(102, 153, 255) };
                    OxyPlot.Axes.LinearAxis xAxis = new LinearAxis { Position = AxisPosition.Bottom, Title = "Numer podejścia do testu", Maximum = sampleCount + 1, Minimum = 0, MinimumMajorStep = 1, MinimumMinorStep = 1 };
                    OxyPlot.Axes.LinearAxis yAxis = new LinearAxis { Position = AxisPosition.Left, Title = "Wynik", Maximum = 3, Minimum = 0, MinimumMajorStep = 1, MinimumMinorStep = 1 };

                    for (int i = 0; i < sampleCount; i++)
                        physicalResult.Points.Add(new DataPoint(i + 1, int.Parse(readDataSplit[i + 2])));

                    this.Model.Axes.Add(xAxis);
                    this.Model.Axes.Add(yAxis);
                    this.Model.Series.Add(physicalResult);
                }
                else if (readDataSplit[0] == "sp")
                {
                    var soundPlaybackResult = new FunctionSeries() { Title = "Odtwarzaniestr. dźwiękowych", Color = OxyColor.FromRgb(0, 0, 153) };
                    OxyPlot.Axes.LinearAxis xAxis = new LinearAxis { Position = AxisPosition.Bottom, Title = "Numer podejścia do testu", Maximum = sampleCount + 1, Minimum = 0, MinimumMajorStep = 1, MinimumMinorStep = 1 };
                    OxyPlot.Axes.LinearAxis yAxis = new LinearAxis { Position = AxisPosition.Left, Title = "Wynik", Maximum = 3, Minimum = 0, MinimumMajorStep = 1, MinimumMinorStep = 1 };

                    for (int i = 0; i < sampleCount; i++)
                        soundPlaybackResult.Points.Add(new DataPoint(i + 1, int.Parse(readDataSplit[i + 2])));

                    this.Model.Axes.Add(xAxis);
                    this.Model.Axes.Add(yAxis);
                    this.Model.Series.Add(soundPlaybackResult);

                }
                else if (readDataSplit[0] == "sa")
                {
                    var sentenceResult = new FunctionSeries() { Title = "Analiza zdaniowa", Color = OxyColor.FromRgb(255, 102, 255) };
                    OxyPlot.Axes.LinearAxis xAxis = new LinearAxis { Position = AxisPosition.Bottom, Title = "Numer podejścia do testu", Maximum = sampleCount + 1, Minimum = 0, MinimumMajorStep = 1, MinimumMinorStep = 1 };
                    OxyPlot.Axes.LinearAxis yAxis = new LinearAxis { Position = AxisPosition.Left, Title = "Wynik", Maximum = 3, Minimum = 0, MinimumMajorStep = 1, MinimumMinorStep = 1 };

                    for (int i = 0; i < sampleCount; i++)
                        sentenceResult.Points.Add(new DataPoint(i + 1, int.Parse(readDataSplit[i + 2])));

                    this.Model.Axes.Add(xAxis);
                    this.Model.Axes.Add(yAxis);
                    this.Model.Series.Add(sentenceResult);
                }
                else if (readDataSplit[0] == "rh")
                {
                    var rhymeResult = new FunctionSeries() { Title = "Tworzenie i rozpoznawanie rymów", Color = OxyColor.FromRgb(102, 0, 102) };
                    OxyPlot.Axes.LinearAxis xAxis = new LinearAxis { Position = AxisPosition.Bottom, Title = "Numer podejścia do testu", Maximum = sampleCount + 1, Minimum = 0, MinimumMajorStep = 1, MinimumMinorStep = 1 };
                    OxyPlot.Axes.LinearAxis yAxis = new LinearAxis { Position = AxisPosition.Left, Title = "Wynik", Maximum = 3, Minimum = 0, MinimumMajorStep = 1, MinimumMinorStep = 1 };

                    for (int i = 0; i < sampleCount; i++)
                        rhymeResult.Points.Add(new DataPoint(i + 1, int.Parse(readDataSplit[i + 2])));

                    this.Model.Axes.Add(xAxis);
                    this.Model.Axes.Add(yAxis);
                    this.Model.Series.Add(rhymeResult);
                }
                else
                    this.Model.Series.Add(data = new FunctionSeries(Math.Tan, 0, 10, 0.1));
            }
            else
                this.Model = new PlotModel { Title = "Coś poszło nie tak" };

            this.Model.InvalidatePlot(true);
        }
        public PlotModel Model { get; private set; }
    }
}