﻿using System.Windows;

namespace PolewczykZofia_projektInzynierski
{
    public partial class ExerciseChoice : Window
    {
        readonly bool test = false;
        readonly int[] score = null;
        readonly string patientName = null;
        public ExerciseChoice()
        {
            InitializeComponent();
        }

        private void PhysicalHearingExerciseButton_Click(object sender, RoutedEventArgs e)
        {
            PhysicalHearingExercise physicalHearingExercise = new PhysicalHearingExercise(test, score, patientName);
            physicalHearingExercise.Show();
        }

        private void SoundPlaybackExerciseButton_Click(object sender, RoutedEventArgs e)
        {
            SoundPlaybackExercise soundPlaybackExercise = new SoundPlaybackExercise(test, score, patientName);
            soundPlaybackExercise.Show();
        }

        private void SentenceAnalysisExerciseButton_Click(object sender, RoutedEventArgs e)
        {
            SentenceAnalysisExercise sentenceAnaliysisExercise = new SentenceAnalysisExercise(test, score, patientName);
            sentenceAnaliysisExercise.Show();
        }

        private void RhymeExerciseButton_Click(object sender, RoutedEventArgs e)
        {
            RhymeExercise rhymeExercise = new RhymeExercise(test, score, patientName);
            rhymeExercise.Show();
        }
    }
}