﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;

namespace PolewczykZofia_projektInzynierski
{
    public partial class Statistics : Window
    {
        public Statistics()
        {
            InitializeComponent();
            string[] dataChoice = new string[] { "Wyniki testu", "Wyniki ćwiczenia słuchu fizycznego", "Wyniki ćwiczenia odtwarzania struktur dźwiękowych", "Wyniki ćwiczenia analizy zdaniowej", "Wyniki ćwiczenia tworzenia i rozpoznawania rymów" };
            dataComboBox.ItemsSource = dataChoice;

            StreamWriter streamWriter = File.AppendText("Pacjenci.txt");
            streamWriter.Flush();
            streamWriter.Close();
            StreamReader streamReader = new StreamReader("Pacjenci.txt");
            string patients = streamReader.ReadToEnd();
            string[] patientsInfo = patients.Split(' ');
            string[] patientsNames = new string[patientsInfo.Length];
            for (int i = 1; i < patientsInfo.Length; i += 2)
                patientsNames[i] = patientsInfo[i];

            patientsNames = patientsNames.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            streamReader.Close();
            patientsNamesComboBox.ItemsSource = patientsNames;
        }

        private void ShowButton_Click(object sender, RoutedEventArgs e)
        {
            if (patientsNamesComboBox.SelectedItem != null && dataComboBox.SelectedItem != null)
            {
                infoTextBlock.Text = "";
                StreamWriter streamWriter = new StreamWriter("RezultatyTestu.txt", true);
                streamWriter.Close();
                StreamReader streamReader = new StreamReader("RezultatyTestu.txt");
                string patients = streamReader.ReadToEnd();
                string[] patientsInfo = patients.Split(' ');
                streamReader.Close();

                List<string> selectedPatientData = new List<string>(),
                    testDate = new List<string>();
                List<int> testResult = new List<int>(),
                    PhysicalHearingResult = new List<int>(),
                    SoundPlaybackResult = new List<int>(),
                    RhymeResult = new List<int>(),
                    SentenceResult = new List<int>();

                int j = 1;
                for (int i = 0; i < (patientsInfo.Length - 1) / 8; i++)
                {
                    if (patientsInfo[j] == patientsNamesComboBox.SelectedItem.ToString())
                    {
                        for (int k = 1; k <= 6; k++)
                            selectedPatientData.Add(patientsInfo[j + k]);
                    }
                    j += 8;
                }

                if (selectedPatientData.Count <=6)
                {
                    if(selectedPatientData.Count == 0)
                        infoTextBlock.Text = "Brak danych dla wybranego pacjenta";
                    else
                        infoTextBlock.Text = "Brak wystarczających danych dla wybranego pacjenta (min. 2 testy)";
                }
                else
                {
                    j = 0;
                    for (int i = 0; i < selectedPatientData.Count / 6; i++)
                    {
                        PhysicalHearingResult.Add(int.Parse(selectedPatientData[j]));
                        j++;
                        SoundPlaybackResult.Add(int.Parse(selectedPatientData[j]));
                        j++;
                        SentenceResult.Add(int.Parse(selectedPatientData[j]));
                        j++;
                        RhymeResult.Add(int.Parse(selectedPatientData[j]));
                        j++;
                        testResult.Add(int.Parse(selectedPatientData[j]));
                        j++;
                        testDate.Add(selectedPatientData[j]);
                        j++;
                    }

                    if (dataComboBox.SelectedItem.ToString() == "Wyniki testu")
                    {
                        streamWriter = new StreamWriter("DanePacjenta.txt");
                        streamWriter.Write("test \n " + String.Join(" ", testResult.ToArray()) + " \n" + " " + String.Join(" ", testDate.ToArray()));
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                    else if (dataComboBox.SelectedItem.ToString() == "Wyniki ćwiczenia słuchu fizycznego")
                    {
                        streamWriter = new StreamWriter("DanePacjenta.txt");
                        streamWriter.Write("ph \n " + String.Join(" ", PhysicalHearingResult.ToArray()));
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                    else if (dataComboBox.SelectedItem.ToString() == "Wyniki ćwiczenia odtwarzania struktur dźwiękowych")
                    {
                        streamWriter = new StreamWriter("DanePacjenta.txt");
                        streamWriter.Write("sp \n " + String.Join(" ", SoundPlaybackResult.ToArray()));
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                    else if (dataComboBox.SelectedItem.ToString() == "Wyniki ćwiczenia analizy zdaniowej")
                    {
                        streamWriter = new StreamWriter("DanePacjenta.txt");
                        streamWriter.Write("sa \n " + String.Join(" ", SentenceResult.ToArray()));
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                    else if (dataComboBox.SelectedItem.ToString() == "Wyniki ćwiczenia tworzenia i rozpoznawania rymów")
                    {
                        streamWriter = new StreamWriter("DanePacjenta.txt");
                        streamWriter.Write("rh \n " + String.Join(" ", RhymeResult.ToArray()));
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                    PlotWindow plotWindow = new PlotWindow();
                    plotWindow.Show();
                }
            }
            else
                infoTextBlock.Text = "Nie wybrano pacjenta lub typu danych";
        }

        private void DataComboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            infoTextBlock.Text = "";
        }

        private void PatientsNamesComboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            infoTextBlock.Text = "";
        }
    }
}
