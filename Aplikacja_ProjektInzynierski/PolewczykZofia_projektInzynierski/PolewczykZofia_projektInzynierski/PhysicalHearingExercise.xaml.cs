﻿using System;
using System.Media;
using System.Windows;
using System.Windows.Media.Imaging;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace PolewczykZofia_projektInzynierski
{
    public partial class PhysicalHearingExercise : Window
    {
        readonly string[] examples = new string[] {"bell", "cat", "clock", "dog", "hairdryer", "mower", "newspaper", "parrot", "phone", "water","poppy", 
                                          "pear", "log", "hook", "hammer", "fence", "boat", "can", "elephant", "horse", "lock" };
        readonly Random random = new Random();
        List<int> examplesList = new List<int> { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        SoundPlayer player;
        private int correctAnwser = 0, counter = 0, soundExample;
        private string anwserImageUri = null;
        private readonly string patientName;
        readonly bool test;
        readonly int[] score;

        public PhysicalHearingExercise(bool test, int[] score, string patientName)
        {
            InitializeComponent();
            this.test = test;
            this.score = score;
            this.patientName = patientName;
            if (test == true)
                newExampleButton.Visibility = Visibility.Hidden;

            examplesList = examplesList.OrderBy(x => Guid.NewGuid()).ToList();
            soundExample = examplesList.First();
            correctAnwser = random.Next(0, 3);

            HearingExerciseLoop hearingExerciseLoop = new HearingExerciseLoop();
            int[] anwserNumber = hearingExerciseLoop.NewExample(examplesList, examples, correctAnwser);

            anwserImageUri = "/images/" + examples[anwserNumber[0]] + ".jpg";
            buttonOneImage.Source = new BitmapImage(new Uri(anwserImageUri, UriKind.Relative));

            anwserImageUri = "/images/" + examples[anwserNumber[1]] + ".jpg";
            buttonTwoImage.Source = new BitmapImage(new Uri(anwserImageUri, UriKind.Relative));

            anwserImageUri = "/images/" + examples[anwserNumber[2]] + ".jpg";
            buttonThreeImage.Source = new BitmapImage(new Uri(anwserImageUri, UriKind.Relative));
            examplesList.Remove(examplesList.First());
        }

        private void NewExampleButton_Click(object sender, RoutedEventArgs e)
        {
            if (!examplesList.Any())
            {
                examplesList = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
                examplesList = examplesList.OrderBy(x => Guid.NewGuid()).ToList();
            }
            soundExample = examplesList.First();
            correctAnwser = random.Next(0, 3);
            HearingExerciseLoop hearingExerciseLoop = new HearingExerciseLoop();
            int[] anwserNumber = hearingExerciseLoop.NewExample(examplesList, examples, correctAnwser);

            anwserImageUri = "/images/" + examples[anwserNumber[0]] + ".jpg";
            buttonOneImage.Source = new BitmapImage(new Uri(anwserImageUri, UriKind.Relative));

            anwserImageUri = "/images/" + examples[anwserNumber[1]] + ".jpg";
            buttonTwoImage.Source = new BitmapImage(new Uri(anwserImageUri, UriKind.Relative));

            anwserImageUri = "/images/" + examples[anwserNumber[2]] + ".jpg";
            buttonThreeImage.Source = new BitmapImage(new Uri(anwserImageUri, UriKind.Relative));
            examplesList.Remove(examplesList.First());
        }

        private void SoundButton_Click(object sender, RoutedEventArgs e)
        {
            Stream[] sounds = new Stream[] { Properties.Resources.bell, Properties.Resources.cat, Properties.Resources.clock, Properties.Resources.dog, Properties.Resources.hairdryer,
                                            Properties.Resources.mower, Properties.Resources.newspaper, Properties.Resources.parrot, Properties.Resources.phone, Properties.Resources.water};
            player = new SoundPlayer(sounds[soundExample]);
            player.Play();
        }

        private void InstructionButton_Click(object sender, RoutedEventArgs e)
        {
            player = new SoundPlayer(Properties.Resources.PhysicalhearingInstr);
            player.Play();
        }

        private void ButtonOne_Click(object sender, RoutedEventArgs e)
        {
            if (test == false)
            {
                if (correctAnwser == 0)
                {
                    buttonOneImage.Source = new BitmapImage(new Uri("/images/correct.jpg", UriKind.Relative));
                    player = new SoundPlayer(Properties.Resources.correct);
                    player.Play();
                }
                else
                {
                    buttonOneImage.Source = new BitmapImage(new Uri("/images/wrong.jpg", UriKind.Relative));
                    player = new SoundPlayer(Properties.Resources.wrong);
                    player.Play();
                }
            }
            else
            {
                if (correctAnwser == 0)
                {
                    score[counter] = 1;
                    counter++;
                    if (counter < 2)
                        NewExampleButton_Click(sender, e);
                    else
                    {
                        SoundPlaybackExercise soundPlaybackExercise = new SoundPlaybackExercise(test, score, patientName);
                        soundPlaybackExercise.Show();
                        Close();
                    }
                }
                else
                {
                    counter++;
                    if (counter < 2)
                        NewExampleButton_Click(sender, e);
                    else
                    {
                        SoundPlaybackExercise soundPlaybackExercise = new SoundPlaybackExercise(test, score, patientName);
                        soundPlaybackExercise.Show();
                        Close();
                    }
                }
            }
        }

        private void ButtonTwo_Click(object sender, RoutedEventArgs e)
        {
            if (test == false)
            {
                if (correctAnwser == 1)
                {
                    buttonTwoImage.Source = new BitmapImage(new Uri("/images/correct.jpg", UriKind.Relative));
                    player = new SoundPlayer(Properties.Resources.correct);
                    player.Play();
                }
                else
                {
                    buttonTwoImage.Source = new BitmapImage(new Uri("/images/wrong.jpg", UriKind.Relative));
                    player = new SoundPlayer(Properties.Resources.wrong);
                    player.Play();
                }
            }
            else
            {
                if (correctAnwser == 1)
                {
                    score[counter] = 1;
                    counter++;
                    if (counter < 2)
                        NewExampleButton_Click(sender, e);
                    else
                    {
                        SoundPlaybackExercise soundPlaybackExercise = new SoundPlaybackExercise(test, score, patientName);
                        soundPlaybackExercise.Show();
                        Close();
                    }
                }
                else
                {
                    counter++;
                    if (counter < 2)
                        NewExampleButton_Click(sender, e);
                    else
                    {
                        SoundPlaybackExercise soundPlaybackExercise = new SoundPlaybackExercise(test, score, patientName);
                        soundPlaybackExercise.Show();
                        Close();
                    }
                }
            }
        }

        private void ButtonThree_Click(object sender, RoutedEventArgs e)
        {
            if (test == false)
            {
                if (correctAnwser == 2)
                {
                    buttonThreeImage.Source = new BitmapImage(new Uri("/images/correct.jpg", UriKind.Relative));
                    player = new SoundPlayer(Properties.Resources.correct);
                    player.Play();
                }
                else
                {
                    buttonThreeImage.Source = new BitmapImage(new Uri("/images/wrong.jpg", UriKind.Relative));
                    player = new SoundPlayer(Properties.Resources.wrong);
                    player.Play();
                }
            }
            else
            {
                if (correctAnwser == 2)
                {
                    score[counter] = 1;
                    counter++;
                    if (counter < 2)
                        NewExampleButton_Click(sender, e);
                    else
                    {
                        SoundPlaybackExercise soundPlaybackExercise = new SoundPlaybackExercise(test, score, patientName);
                        soundPlaybackExercise.Show();
                        Close();
                    }
                }
                else
                {
                    counter++;
                    if (counter < 2)
                        NewExampleButton_Click(sender, e);
                    else
                    {
                        SoundPlaybackExercise soundPlaybackExercise = new SoundPlaybackExercise(test, score, patientName);
                        soundPlaybackExercise.Show();
                        Close();
                    }
                }
            }  
        }
    }
}