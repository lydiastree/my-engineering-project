﻿using System.Windows;

namespace PolewczykZofia_projektInzynierski
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ExerciseButton_Click(object sender, RoutedEventArgs e)
        {
            ExerciseChoice exerciseChoice = new ExerciseChoice();
            exerciseChoice.Show();
        }

        private void TestButton_Click(object sender, RoutedEventArgs e)
        {
            TestLogging testLogging = new TestLogging();
            testLogging.Show();
        }
    }
}
