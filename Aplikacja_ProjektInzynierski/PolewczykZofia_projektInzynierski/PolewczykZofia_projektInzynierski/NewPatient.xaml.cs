﻿using System;
using System.Linq;
using System.Windows;
using System.IO;
using System.Windows.Media;

namespace PolewczykZofia_projektInzynierski
{
    public partial class NewPatient : Window
    {
        public NewPatient()
        {
            InitializeComponent();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                StreamWriter streamWriter = new StreamWriter("Pacjenci.txt", true);
                streamWriter.Close();
                StreamReader streamReader = new StreamReader("Pacjenci.txt");
                string patients = streamReader.ReadToEnd();
                string[] patientsInfo = patients.Split(' ');
                streamReader.Close();

                if (patientsInfo.Contains(nameTextBox.Text))
                {
                    infoTextBlock.Foreground = Brushes.Red;
                    infoTextBlock.Text = "Podane imię lub login jest już zajęte";
                }
                else if (birthDate.SelectedDate.Value.Date > DateTime.Now.Date)
                {
                    infoTextBlock.Foreground = Brushes.Red;
                    infoTextBlock.Text = "Podana data jest nieprawidłowa";
                }
                else
                {
                    if (nameTextBox != null && birthDate.SelectedDate != null)
                    {
                        streamWriter = new StreamWriter("Pacjenci.txt", true);
                        string patientData = " " + nameTextBox.Text + " " + birthDate.SelectedDate.Value.Date.ToShortDateString() + "\n";
                        streamWriter.Write(patientData);
                        streamWriter.Flush();
                        streamWriter.Close();
                        infoTextBlock.Foreground = Brushes.Green;
                        infoTextBlock.Text = "Dodano pacjenta";
                    }
                    else
                    {
                        infoTextBlock.Foreground = Brushes.Red;
                        infoTextBlock.Text = "Sprawdź poprawność wpisywanych danych";
                    }
                }
            }
            catch (Exception)
            {
                infoTextBlock.Foreground = Brushes.Red;
                infoTextBlock.Text = "Coś poszło nie tak :(";
            }
        }

        private void NameTextBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            infoTextBlock.Foreground = Brushes.White;
            infoTextBlock.Text = "";
        }
    }
}