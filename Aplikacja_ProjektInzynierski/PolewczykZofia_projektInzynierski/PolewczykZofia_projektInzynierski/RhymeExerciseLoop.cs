﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PolewczykZofia_projektInzynierski
{
    class RhymeExerciseLoop
    {
        readonly Random random = new Random();
        int numberHelper;
        readonly string[] usedExamples = new string[5];
        readonly int[] anwserNumber = new int[4];

        public int[] NewExample(List<int> examplesList, string[,] examples, int correctAnwser)
        {
            usedExamples[3] = examples[0, examplesList.First()]; 
            usedExamples[4] = examples[1, examplesList.First()]; 
            anwserNumber[3] = examplesList.First(); 

            for (int i = 0; i < anwserNumber.Length; i++)
            {
                do
                {
                    numberHelper = random.Next(0, examples.Length/2);
                    if (anwserNumber.Contains(numberHelper) || examples[0, examplesList.First()] == examples[1, numberHelper] || usedExamples.Contains(examples[1, numberHelper]))
                        numberHelper = examplesList.First();

                }
                while (numberHelper == examplesList.First());
                anwserNumber[i] = numberHelper;
                usedExamples[i] = examples[1, numberHelper];
             }
             anwserNumber[correctAnwser] = examplesList.First();
            return anwserNumber;
        }
    }
}